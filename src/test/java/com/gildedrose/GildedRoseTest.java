package com.gildedrose;

import com.gildedrose.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void foo() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].getName());
    }

    static Stream<Arguments> getItems() {
        return Stream.of(
            //region Before SellIn date is passed
            Arguments.of(new Item("+5 Dexterity Vest", 10, 20), 9, 19),
            Arguments.of(new Item("Aged Brie", 2, 0), 1, 1),
            Arguments.of(new Item("Elixir of the Mongoose", 5, 7), 4, 6),
            Arguments.of(new Item("Sulfuras, Hand of Ragnaros", 0, 80), 0, 80),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20), 14, 21),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 11, 40), 10, 41),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 40), 9, 42),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49), 9, 50),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 6, 40), 5, 42),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 40), 4, 43),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49), 4, 50),
            Arguments.of(new Item("Conjured Mana Cake", 3, 6), 2, 4),
            //endregion
            //region After Sellin date is passed
            Arguments.of(new Item("+5 Dexterity Vest", 0, 20), -1, 18),
            Arguments.of(new Item("Aged Brie", 0, 0), -1, 2),
            Arguments.of(new Item("Elixir of the Mongoose", 0, 7), -1, 5),
            Arguments.of(new Item("Sulfuras, Hand of Ragnaros", -1, 80), -1, 80),
            Arguments.of(new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20), -1, 0),
            Arguments.of(new Item("Conjured Mana Cake", 0, 6), -1, 2),
            //endregion
            //region Value can't drop below zero
            Arguments.of(new Item("+5 Dexterity Vest", 10, 1), 9, 0),
            Arguments.of(new Item("+5 Dexterity Vest", 10, 0), 9, 0),
            Arguments.of(new Item("+5 Dexterity Vest", -1, 2), -2, 0),
            Arguments.of(new Item("+5 Dexterity Vest", -1, 1), -2, 0),
            Arguments.of(new Item("Elixir of the Mongoose", 5, 0), 4, 0),
            Arguments.of(new Item("Conjured Mana Cake", 3, 1), 2, 0),
            Arguments.of(new Item("Conjured Mana Cake", 3, 0), 2, 0),
            Arguments.of(new Item("Conjured Mana Cake", -1, 4), -2, 0),
            Arguments.of(new Item("Conjured Mana Cake", -1, 3), -2, 0),
            Arguments.of(new Item("Conjured Mana Cake", -1, 2), -2, 0),
            Arguments.of(new Item("Conjured Mana Cake", -1, 1), -2, 0),
            Arguments.of(new Item("Conjured Mana Cake", -1, 0), -2, 0),
            //endregion
            //region Value can't increase above 50
            Arguments.of(new Item("Aged Brie", 2, 49), 1, 50),
            Arguments.of(new Item("Aged Brie", 2, 50), 1, 50),
            Arguments.of(new Item("Aged Brie", -1, 48), -2, 50),
            Arguments.of(new Item("Aged Brie", -1, 49), -2, 50)
            //endregion
        );
    }

    @ParameterizedTest(name = "{displayName} - [{index}] {arguments}")
    @MethodSource("getItems")
    void testItemsUpdateQuality(Item actualItem, int expectedSellIn, int expectedQuality) {
        //given
        GildedRose actualGildedRose = new GildedRose(new Item[]{actualItem});
        //when
        actualGildedRose.updateQuality();
        //then
        assertThat(actualGildedRose.items[0])
            .isNotNull()
            .hasNoNullFieldsOrProperties()
            .hasFieldOrPropertyWithValue("sellIn", expectedSellIn)
            .hasFieldOrPropertyWithValue("quality", expectedQuality);
    }
}
