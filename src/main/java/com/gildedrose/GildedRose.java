package com.gildedrose;

import com.gildedrose.model.Item;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class GildedRose {
    Item[] items;

    public void updateQuality() {
        for (Item item : items) {
            if (item.getName().equals("Sulfuras, Hand of Ragnaros")) {
                return;
            }

            if (isAgedBrie(item)) {
                increaseQuality(item);
            } else if (isBackstagePasses(item)) {
                increaseQuality(item);

                if (item.getSellIn() < 11) {
                    increaseQuality(item);
                }
                if (item.getSellIn() < 6) {
                    increaseQuality(item);
                }
            } else if (isConjuredItem(item)) {
                decreaseQuality(item);
                decreaseQuality(item);
            } else {
                decreaseQuality(item);
            }

            item.setSellIn(item.getSellIn() - 1);

            if (item.getSellIn() < 0) {
                if (isAgedBrie(item)) {
                    increaseQuality(item);
                } else if (isBackstagePasses(item)) {
                    item.setQuality(0);
                } else if (isConjuredItem(item)) {
                    decreaseQuality(item);
                    decreaseQuality(item);
                } else {
                    decreaseQuality(item);
                }
            }
        }
    }

    private void decreaseQuality(Item item) {
        if (item.getQuality() > 0) {
            item.setQuality(item.getQuality() - 1);
        }
    }

    private void increaseQuality(Item item) {
        if (item.getQuality() < 50) {
            item.setQuality(item.getQuality() + 1);
        }
    }

    private boolean isAgedBrie(Item item) {
        return item.getName().equals("Aged Brie");
    }

    private boolean isBackstagePasses(Item item) {
        return item.getName().equals("Backstage passes to a TAFKAL80ETC concert");
    }

    private boolean isConjuredItem(Item item) {
        return item.getName().matches("^Conjured.*");
    }
}
